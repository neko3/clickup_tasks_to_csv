import requests
import json
import sys
from datetime import datetime as dt

def send_get_req(url, headers):
    r = requests.get(url, headers=headers)
    req_dict = json.loads(r.text)
    return req_dict

def get_tasks(filter_lists=None, _debug=False):
    my_tasks = {}
    def sub_get_tasks():
        list_id = list_item['id']
        my_tasks[list_item['name']] = {}
        if _debug: print(' ' * 12, list_id, list_item['name'])
        ## get tasks
        page = 0
        while True:
            tasks_list = (send_get_req(f'https://api.clickup.com/api/v2/list/{list_id}/task?archived=false&page={page}&subtasks=true&include_closed=true', headers))['tasks']
            for task in tasks_list:
                task_id = task['id']
                if _debug: print(' ' * 16, task_id, task['name'], task['parent'])
                if task['parent'] is None:
                    if task['id'] in my_tasks[list_item['name']].keys():
                        ## it has a placeholder
                        my_tasks[list_item['name']][task['id']]['body'] = task
                    else:
                        my_tasks[list_item['name']][task['id']] = {'body': task, 'subtasks': []}
                else:
                    ## find parent task
                    if task['parent'] in my_tasks[list_item['name']].keys():
                        my_tasks[list_item['name']][task['parent']]['subtasks'].append(task)
                    else:
                        ## make a placeholder:
                        my_tasks[list_item['name']][task['parent']] = {'body': None, 'subtasks': [task]}
                
            if len(tasks_list) < 100:
                break
            else:
                page += 1
                
    ## get teams
    teams_list = (send_get_req(teams_url, headers))['teams']
    for team in teams_list:
        team_id = team['id']
        if _debug: print(team_id, team['name'])
        ## get spaces
        spaces_list = (send_get_req(f'https://api.clickup.com/api/v2/team/{team_id}/space?archived=false', headers))['spaces']
        for space in spaces_list:
            space_id = space['id']
            if _debug: print(' ' * 4, space_id, space['name'])
            ## get folders
            folders_list = (send_get_req(f'https://api.clickup.com/api/v2/space/{space_id}/folder?archived=false', headers))['folders']
            for folder in folders_list:
                folder_id = folder['id']
                if _debug: print(' ' * 8, folder_id, folder['name'])
                ## get lists
                lists_list = (send_get_req(f'https://api.clickup.com/api/v2/folder/{folder_id}/list?archived=false', headers))['lists']
                for list_item in lists_list:
                    if filter_lists is None:
                        print(list_item['name'])
                        sub_get_tasks()
                    else:
                        for filter_list in filter_lists:
                            if filter_list in list_item['name']:
                                print(list_item['name'])
                                sub_get_tasks()
    return my_tasks


if __name__ == '__main__':
    
    if len(sys.argv) < 2:
        filename = 'clickup.csv'
    else:
        filename = sys.argv[1]
        
    _debug = False

    ## get your token from Settings -> Apps!
    token = ''
    ## check auth & session ID if it doesn't work
    headers = {
        'Content-Type': 'application/json', \
        'Authorization': token }
    teams_url = 'https://api.clickup.com/api/v2/team'
    # spaces_url = 'https://api.clickup.com/api/v2/team/team_id/space?archived=false'
    # folders_url = 'https://api.clickup.com/api/v2/space/space_id/folder?archived=false'
    # lists_url = 'https://api.clickup.com/api/v2/folder/folder_id/list?archived=false'
    # tasks_url_one = 'https://api.clickup.com/api/v2/list/list_id/task?archived=false&page=&subtasks=true&include_closed=true' 
    
    my_tasks = get_tasks()
    # my_tasks = get_tasks(['ListNameGoesHere'])
    
    
    priorities = {
        'low': '4', \
        'normal': '3', \
        'high': '2', \
        'urgent': '1'
    }
    
    fmt = "%d/%m/%Y"
    
    with open(filename, 'w') as file:
        if _debug: print('"Priority", "Task", "Due Date", "Description/Subtasks", "Status"')
        file.write('"Priority", "Task", "Due Date", "Description/Subtasks", "Status"\n')
        for list_name, list_tasks in my_tasks.items():
            file.write(f'{list_name}\n')
            for task_id, task_body in list_tasks.items():
                task = task_body['body']
                task_desc = '""' if task['description'] is None else '"' + task['description'].rstrip() + '"'
                epoch = int(task['due_date'])/1000 if task['due_date'] is not None else None
                if not epoch is None:
                    t = dt.fromtimestamp(epoch)
                if _debug: print(f"{'4' if task['priority'] is None else priorities[task['priority']['priority']]}, \"{task['name']}\", {' ' if epoch is None else t.strftime(fmt)}, {task_desc}, {task['status']['status'].lower()}")
                file.write(f"{'4' if task['priority'] is None else priorities[task['priority']['priority']]}, \"{task['name']}\", {' ' if epoch is None else t.strftime(fmt)}, {task_desc}, {task['status']['status'].lower()}\n")                
                if len(task_body['subtasks']) > 0:
                    for subtask in task_body['subtasks']:
                        subtask_desc = '"' + subtask['name']
                        subtask_desc += '"' if subtask['description'] is None or len(subtask['description']) == 1 else ": " + subtask['description'].rstrip() + '"'
                        subtask_epoch = int(subtask['due_date'])/1000 if subtask['due_date'] is not None else None
                        if not subtask_epoch is None:
                            subtask_t = dt.fromtimestamp(subtask_epoch)
                        if _debug: print(f"{'4' if subtask['priority'] is None else priorities[subtask['priority']['priority']]}, \"{task['name']}\", {' ' if subtask_epoch is None else subtask_t.strftime(fmt)}, {subtask_desc}, {subtask['status']['status'].lower()}")
                        file.write(f"{'4' if subtask['priority'] is None else priorities[subtask['priority']['priority']]}, \"{task['name']}\", {' ' if subtask_epoch is None else subtask_t.strftime(fmt)}, {subtask_desc}, {subtask['status']['status'].lower()}\n")
                        