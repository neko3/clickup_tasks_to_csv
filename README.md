## ClickUp tasks to CSV

This is a script to retrieve the tasks of ClickUp lists and export them to CSV. 

You can let it export all lists from all folders and workspaces or you
can give it some keywords that match your lists names and it will only
export those. Check the `main()` function.

Currently, it prints the Priority, Task, Due Date, Description/Subtasks 
and Status. For subtasks, it will use the parent task name in the Task column
and put the `subtask name: subtask description` in the Description/Subtasks
column.
